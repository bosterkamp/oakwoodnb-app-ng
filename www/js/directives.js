/*angular.module('app.directives', [])

.directive('blankDirective', [function(){

}]);
*/

angular.module('app.directives', [])

.directive('activePageHighlight', ['$rootScope', '$state', function($rootScope, $state){

   return function ($scope, $element, $attr) {
       
     function checkUISref(){
         
         //var url = $element.find('a').attr('href');
         //console.info('URL: ' + url);
         //console.info('state = ' + $state.current.name + ': uiSref = ' + $attr['uiSref']);
       if ($attr['uiSref'].indexOf($state.current.name+'(') == 0) {
           //console.info('FOUND: state = ' + $state.current.name + ': uiSref = ' + $attr['uiSref']);
             $element.addClass('active-page-highlight');
         } else {
             //console.info('NOT FOUND: state = ' + $state.current.name + ': uiSref = ' + $attr['uiSref']);
             $element.removeClass('active-page-highlight');
         }
     }
     
     checkUISref();
       
     $rootScope.$on('$stateChangeSuccess', function(){
         checkUISref();
     })
   };

}]);